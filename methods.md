# Methods used for the 1985-1988, 1991-1994 sampling:

Samples for epilithon standing stock and chlorophyll a are collected using a stratified random sampling design based on the number of habitat types (channel units) having substantially different epilithon biomass or type. Within each habitat type that contains epilithon we need to collect a number of random samples within the study reach to calculate an average epilithon standing stock value for each habitat type. However, the sample collection procedures differ depending on the habitat type. For bedrock habitats, use a small cylinder of known area with a foam gasket attached to one end as the sampling template. Push this template firmly against bedrock, vigorously scrape material using a wire brush, and suction scraped material into a bottle (using a turkey baster or large plastic syringe with tip cut off). For rocks that can be picked up, choose 1-5 rocks (equivalent to a total planar area of about 100-200 cm<sup>2</sup>) and brush the rock surface thoroughly with a wire brush to loosen the epilithon and slurry the loosened material into a container. After the epilithon has been removed, measure the approximate planar area of each rock brushed by tracing on a piece of paper (use paper from the same batch that has the same weight per unit area), cutting the tracings and weighing them. If you are brushing more than 1 rock to obtain the desired sample area, combine all rocks collected at a particular sampling location into a single “pooled slurry” sample. For fine-grained sediments the epilithon will be sampled as FBOM, unless it exits as a mat in which case it will be included as filamentous algae.

In the laboratory the total volume of each slurry sample is determined and recorded. Then one well-mixed subsample is measured out and filtered through a pre-combusted and tared glass fiber filter (47-mm diameter, Whatman GFF). This filter is then dried at 60 degrees C, weighed to determine dry mass, combusted (500 degrees C) and reweighed to determine ash-free dry mass. To compute standing stocks of epilithon for the entire stream, weight the averages for each habitat type by the relative proportions of each habitat type in the study reach. Calculate all epilithon standing stocks both in terms of AFDM and dry mass per unit area. 

A second well-mixed subsample of the slurry is then measured out and filtered through another pre-combusted, 47-mm diameter GFF filter for chlorophyll a determination (place filter in aluminum foil packet, label, and freeze until analysis). 
 
## Chlorophyll a measurement

Chlorophyll a will be determined by hot ethanol extraction using the method of Sartory and Grobbelaar (Sartory, D. P. and J. E. Grobbelaar. 1984. Extraction of chlorophyll a from freshwater phytoplankton for spectrophotometric analysis. Hydrobiologia 114:177-187). Keep samples in the dark at all times, at least in low light when working on them. If the sample is kept in the dark, only 1.3% of the chlorophyll degrades with the 5 min. hot extraction and 24-hour storage. Use test tubes that have the tube numbers scribed on the side with a diamond pencil for filter extraction. Place the filter, scrapped material from known area, or even an entire rock, in a container with a known volume (10 mL ethanol in screw cap tubes works well) of 95% ethanol. Mark the location of the meniscus on the side of the tube with sharpie, and place a marble or loose cap on top of the tube. Heat the tube at 79 degrees C for 5 minutes, then mix and cool for 24 h in the dark (can be at room temperature, and can be sealed once cooled if screw cap tubes are used). After extraction, use additional 95% ethanol to bring up to mark on side of tube if ethanol has evaporated, then mix. Clear sample by centrifugation, filtration, or settling. Analyze sample with spectrophotometric analysis at 665 and 750 nm using a 1 cm spectrophotometer cuvette (method in Standard Methods with different extinction coefficient for ethanol and conversion for chlorophyll per unit area). If adsorption is over 1.5 absorbance units, dilute sample. Add 0.1 mL of 0.1 N HCl for each 10 mL of extractant after the first reading and let sample sit for 90 s to phaeophytinize all chlorophyll before reading.
 
# Methods used for the 2010-2013 sampling:
 
## Field Procedure

Each algal type with an estimated percent cover ≥20% was collected (n=5 per type) within each reach. Samples were collected using a plexiglass core measuring 5.6 cm in diameter. Each sample was placed in either a urine cup or whirlpack and kept in the freezer until processed. Five rocks were also collected at random from each stream reach to analyze for algal/ biofilm.
 
## Laboratory Procedure

Sediment samples were divided in half to run for chlorophyll and AFDM. Rock samples were scrubbed in a glass dish using a toothbrush. Excess water in each field sample container was filtered into folded 110 mm GF/A filters into a 200ml beaker. Algae from scrubbed rocks were also poured through GF/A filters. Algae remaining on dish and rocks were washed into filters using as little DI water as possible. Filters were cut in half for analysis of chlorophyll a and AFDM. Sediment along with half of the respective filters was placed together into a 200 mL beaker. Filters from rocks were also placed in 200 mL beakers. 20 mL of reagent grade methanol was added to beakers. Beakers were placed in water bath and allowed to boil at 80-85 degrees C for 2-4 minutes. Next, extract from each sample was poured into separate 100 ml beakers. Methanol addition and boiling was repeated until no green pigment was visible on sediment, filter, or in extract. After final extraction, the total volume of extract collected for each sample was filtered through a GF/F filter (a GF/A was used for samples collected in 2010-2013). Filtered extract was poured into a graduated cylinder. The volume of extract was increased to the nearest 20 ml line by adding methanol. Volume was recorded and samples covered with parafilm to minimize evaporation. Absorbance values of extracts were measured at 750, 666, and 480 nm before and after acidification (see table below) using a Beckman DU® 530 Spectrophotometer.
 
| EXTRACT VOL | ACID ADDITION                           |
|-------------|-----------------------------------------|
| 40 mL       | 2.2 mL [40 mL] HCl                      |
| 60 mL       | 2.2 mL [60 mL] HCl                      |
| 80 mL       | 4.4 mL [40 mL] HCl                      |
| 100 mL      | 2.2 mL [40 mL] HCl + 2.2 mL [60 mL] HCl |
| 120 mL      | 4.4 mL [60 mL] HCl                      |
| 140 mL      | 4.4 ml [40 mL] HCl + 2.2 mL [60 mL] HCl |

Chlorophyll calculations are made as follows:
 
_Chl a (mg/m<sup>2</sup>) = 219. 2(D666-D666a) * v / A_

where 219.2 is the absorption coefficient for chlorophyll a in methanol times the conversion from ug/cm<sup>2</sup> to mg/m<sup>2</sup>; v is extraction volume in mL; A is area of the sampled substrate, 12.315 cm<sup>2</sup>, half the area of collected core; D666 is absorbance at 666 minus absorbance at 750 before acidification, and D666a is absorbance at 666 minus absorbance at 750 after acidification.
 
## AFDM

Remaining gravel and filter samples were placed in labeled aluminum tins and allowed to dry in drying oven at 60 degrees C (celsius degrees) for at least 48 hours. Samples were removed, cooled and weighed for dry mass. Tins were covered individually with aluminum foil and placed in the muffle furnace to ash at 550 degrees C for 2 hours. Ashed samples were allowed to cool, then wet with DI water before placing in the drying oven for 48 hours at 60 degrees C. Samples were weighed again to determine ashed mass.
 
AFDM calculations are made as follows:

_AFDM (g/m<sup>2</sup>) = (Dry mass – Ashed mass)*10,000/ sample area (cm<sup>2</sup>)_

where substrate area = 12.315 cm<sup>2</sup> (half the area of collected core)
