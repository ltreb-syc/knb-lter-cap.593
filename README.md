# knb-lter-cap.593

*title*

Long-term measurements of algal biomass in Sycamore Creek, Arizona, USA

*abstract*

The primary objective of this project is to understand how long-term climate variability and change influence the structure and function of desert streams via effects on hydrologic disturbance regimes. Climate and hydrology are intimately linked in arid landscapes; for this reason, desert streams are particularly well suited for both observing and understanding the consequences of climate variability and directional change. Researchers try to (1) determine how climate variability and change over multiple years influence stream biogeomorphic structure (i.e., prevalence and persistence of wetland and gravel-bed ecosystem states) via their influence on factors that control vegetation biomass, and (2) compare interannual variability in within-year successional patterns in ecosystem processes and community structure of primary producers and consumers of two contrasting reach types (wetland and gravel-bed stream reaches). This dataset was collected to understand changes of algal growth by month during the field season and same season in different years characterized by different hydrological regime. In 2009 and 2013, data was collected in both wetland reach and gravel reach to compare the difference of algal growth in two reach types in the same time of the year. Data collected in 1980s and 1990s include two reach types: riffle and run, and were also to understand the post-flood succession. As sample collection and processing methodologies changed over the course of the long-term study, methods specific to distinct sampling periods are provided.

*notes*

The 'modern' LTREB data include algal biomass data extending 2011 through present. L. Pollard had earlier provided algal biomass data in sets of 1985-1994 and 2010-2013, but those are only as mean chl and afdm values (raw would be better), and it seems that there are inconsistencies between the data L. Pollard provided and the modern data where they overlap (i.e., 2011). Because the raw data prior to 2011 are unavailable at the time of this assessment, and because the inconsistencies among the earlier data and more recent data where they overlap cannot be resolved, knb-lter-cap.593.1 includes only data since 2011. Unlike many of the other LTREB data-publishing work flows that are zipped upon completion, 593 is recorded as a repository owing to future data contributions, both the pre-2011 raw data and new (>2019) data expected to be available.

- 2020-08-03: Algae master is now up-to-date with what I have. None of the 2020 samples have been run yet and there are a few inconsistencies I'm going to try to get someone to check against the raw datasheets for the stuff I just added once I figure out what the status of the lab is.
* As of 2020-06-30, Sophia was going to look into why the Master only had data through 2016. Metadata refer to means and standard errors but these are the raw data, and Sophia was not sure as to the how or the why with the mean and SD data.
* Do not worry about AFDM Chloro FINAL - that should be incorporated into Master.
* A main point here (and likely throughout, really) is to focus on data cleaning. For example, for the P/A data at some point they switched from 1s and 0s to Xs - there will be lots of stuff like that.
* These are dataset 593; however, data in 593 are mean values and there seems to be little or no congruence with the Dropbox data.
* done: published as knb-lter-cap.593.1 in EDI (and in CAP DB) but knb-lter-cap.593.1 includes only data since 2011 - we will not publish at this time the mean values from earlier years that Lindsey provided (waiting on raw) and that we need to resolve discrepencies between the mean values L. Pollard provided and the modern master algal data where they overlap (i.e., 2011).

